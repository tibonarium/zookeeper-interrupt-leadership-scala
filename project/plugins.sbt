logLevel := Level.Warn

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")

// addSbtPlugin("com.sksamuel.scapegoat" %% "sbt-scapegoat" % "1.0.8")
// addSbtPlugin("com.sksamuel.scapegoat" % "sbt-scapegoat-plugin" % "1.3.8")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.6")

addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.2.4")
