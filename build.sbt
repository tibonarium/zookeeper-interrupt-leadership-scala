lazy val commonSettings = Seq(
  organization := "com.altuera.eralink",
  scalaVersion := "2.12.4",
  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature"),
  //wartremoverWarnings ++= Warts.unsafe,
  //scapegoatVersion := "1.3.4",
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case "logback.xml" => MergeStrategy.discard
    case "application.conf" => MergeStrategy.discard
    case PathList(ps@_*) if ps.last endsWith ".sql" => MergeStrategy.discard
    case PathList("org", "slf4j", xs@_*) => MergeStrategy.last
    case x => (assemblyMergeStrategy in assembly).value(x)
  },
  assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false, includeDependency = false)
)

name := "zookeeper-interrupt-leadership"
version := "0.1"
scalaVersion := "2.12.3"

lazy val typesafeConfigVersion = "1.3.3"

lazy val curatorVersion = "4.0.1" // we have problems with LeaderSelector
// lazy val curatorVersion = "4.2.0" // We have same problems with latest version
lazy val zookeeperVersion = "3.4.11"

lazy val logbackVersion = "1.2.3"
lazy val scalaLoggingVersion = "3.8.0"

lazy val loggingDependencies = Seq(
  "ch.qos.logback" % "logback-classic" % logbackVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
)

lazy val curatorDependencies = Seq(

  "org.apache.curator" % "curator-recipes" % curatorVersion excludeAll (
    ExclusionRule(organization = "org.apache.zookeeper")
  ),
  "org.apache.curator" % "curator-x-async" % curatorVersion excludeAll (
    ExclusionRule(organization = "org.apache.zookeeper")
  ),
  "org.apache.curator" % "curator-test" % curatorVersion excludeAll (
    ExclusionRule(organization = "org.apache.zookeeper")
  ),
  "org.apache.zookeeper" % "zookeeper" % zookeeperVersion excludeAll(
    ExclusionRule("log4j", "log4j"),
    ExclusionRule("org.slf4j", "slf4j-log4j12")
  )  
)


lazy val root = (project in file("."))
  .settings(commonSettings).settings(
		version := "0.1",
		name := "zookeeper-interrupt-leadership",
		commonSettings,
		libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3",
		libraryDependencies += "com.typesafe" % "config" % typesafeConfigVersion,
		libraryDependencies ++= curatorDependencies
	)
