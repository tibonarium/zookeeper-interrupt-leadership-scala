
package com.altuera.example

import org.apache.curator.framework.CuratorFramework
import org.apache.curator.framework.recipes.leader.LeaderSelectorListenerAdapter
import org.apache.curator.framework.recipes.leader.LeaderSelector
import java.io.Closeable
import java.io.IOException
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
// apache
import org.apache.curator.utils.ThreadUtils
import java.util.concurrent.Executors
// logger
import org.slf4j.LoggerFactory

class ExampleClient(
  val client: CuratorFramework, 
  val path: String, 
  val name: String,
	submitCallback: () => Unit
) extends LeaderSelectorListenerAdapter with Closeable
{
  val logger = LoggerFactory.getLogger(this.getClass)

  private val leaderCount = new AtomicInteger();
	
	val defaultThreadFactory = ThreadUtils.newThreadFactory("LeaderSelector-test");

  // create a leader selector using the given path for management
  // all participants in a given leader selection must use the same path
  // ExampleClient here is also a LeaderSelectorListener but this isn't required
  
	// val leaderSelector = new LeaderSelector(client, path, this);
	
	val leaderSelector = new LeaderSelector(
		client, 
		path, 
		new CloseableExecutorServiceTest(
			Executors.newSingleThreadExecutor(defaultThreadFactory), 
			true,
			submitCallback
		),
		this)

  // for most cases you will want your instance to requeue when it relinquishes leadership
  leaderSelector.autoRequeue();
  

  @throws[IOException]
  def start(): Unit =
  {
      // the selection for this instance doesn't start until the leader selector is started
      // leader selection is done in the background so this call to leaderSelector.start() returns immediately
      leaderSelector.start();
  }

  @throws[IOException]
  override def close(): Unit =
  {
      leaderSelector.close();
  }

  override def takeLeadership(client: CuratorFramework): Unit =
  {
    // we are now the leader. This method should not return until we want to relinquish leadership

    val waitSeconds = 5 * Math.random() + 1;

    logger.info(name + " is now the leader. Waiting " + waitSeconds + " seconds...");
    logger.info(name + " has been leader " + leaderCount.getAndIncrement() + " time(s) before.");
    try
    {
      Thread.sleep(TimeUnit.SECONDS.toMillis(waitSeconds.toInt))
    }
    catch {
      case e: InterruptedException => {
        logger.info(name + " was interrupted.")
        Thread.currentThread().interrupt()
      }
    }
    finally
    {
      logger.info(name + " relinquishing leadership.\n");
    }
  }
  
  def interruptLeadership(): Unit = {  
    leaderSelector.interruptLeadership()
  }
  
  def correctInterruptLeadership(): Unit = {  
    if( leaderSelector.hasLeadership() )
    {
      leaderSelector.interruptLeadership()
    }
  }
}
