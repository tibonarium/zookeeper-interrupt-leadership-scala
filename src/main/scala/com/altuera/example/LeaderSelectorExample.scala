
package com.altuera.example

import com.google.common.collect.Lists
import org.apache.curator.utils.CloseableUtils
import org.apache.curator.framework.CuratorFramework
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.curator.test.TestingServer
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.List
// logger
import org.slf4j.LoggerFactory
// zookeper
import org.apache.zookeeper.WatchedEvent
// scala
import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger

object LeaderSelectorExample extends App
{
  val logger = LoggerFactory.getLogger(this.getClass)
  val PATH = "/examples/leader"

  val DEFAULT_SESSION_TIMEOUT_MS = Integer.getInteger("curator-default-session-timeout", 60 * 1000);
  val DEFAULT_CONNECTION_TIMEOUT_MS = Integer.getInteger("curator-default-connection-timeout", 15 * 1000);
  
  val connectString = "localhost:2181"
  val retryPolicy = new ExponentialBackoffRetry(1000, 3)
  
  val callCount = new AtomicInteger();
  
  implicit val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))
  
  logger.info("Notice that leader election is fair: all clients will become leader and will do so the same number of times.");

  // Alternative:
  // val server = new TestingServer();
  val client = CuratorFrameworkFactory.newClient(connectString, retryPolicy)

	
  val example = new ExampleClient(
    client = client, 
    path = PATH, 
    name = "Client #" + 1,
  	submitCallback = submitCallback _
  )
  
  try
  {
    client.start()
    example.start()

    logger.info("Press enter/return to interruptLeadership\n")
    new BufferedReader(new InputStreamReader(System.in)).readLine()
    
    example.interruptLeadership() 
    
    // logger.info("Press enter/return to Again interruptLeadership\n")
    // new BufferedReader(new InputStreamReader(System.in)).readLine()
    
    
    logger.info("Press enter/return to close application\n")
    new BufferedReader(new InputStreamReader(System.in)).readLine()
  }
  finally
  {
    logger.info("Shutting down...")

    CloseableUtils.closeQuietly(example)
    CloseableUtils.closeQuietly(client)

    // CloseableUtils.closeQuietly(server);
  }
	
	def submitCallback(): Unit = {
	  val count = callCount.getAndIncrement()
	  logger.info(s"Submit task was called #${count}")
	  
	  // if it will interrupt only once then it is okey
	  // if( count == 3 )
	  if( 3 <= count && count <= 4 )
	  {
	    val future = Future {
	      Thread.sleep(100)
	      
  	    logger.info(s"Call again interruptLeadership")
  	    // If we simply interrupt leadership then autoRequeue stop working correctly
  	    example.interruptLeadership()
  	    
  	    // If we interrupt correctly then problem disappears
  	    // example.correctInterruptLeadership()
	    }
	    
	    Thread.sleep(250)
	  }
	}
    
}
