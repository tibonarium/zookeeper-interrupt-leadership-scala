package com.altuera.example

import java.util.concurrent.ExecutorService
import java.util.concurrent.{ Future => JFuture }
import java.util.concurrent.{ Callable => JCallable }
// curator
import org.apache.curator.utils.CloseableExecutorService

class CloseableExecutorServiceTest(
	executorService: ExecutorService, 
	shutdownOnClose: Boolean, 
	submitCallback: () => Unit
) extends CloseableExecutorService(executorService, shutdownOnClose) {

	override def submit[V](task: JCallable[V]): JFuture[V] = {
		submitCallback()
		return super.submit(task)
	}
}
